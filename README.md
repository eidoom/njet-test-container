# [njet-test-container](https://gitlab.com/eidoom/njet-test-container)

Image with the latest [NJet](https://bitbucket.org/njet/njet) [release](https://bitbucket.org/njet/njet/downloads/).
Source and build files are included.
For testing.

## Get

Container image on [Docker Hub](https://hub.docker.com/r/eidoom/njet-test).

Get it with
```shell
docker pull eidoom/njet-test[:<tag>]
```

Or on Fedora
```shell
podman pull docker.io/eidoom/njet-test[:<tag>]
```
Similarly replace `docker` with `podman` for the following commands.

## Use

Interactive:
```shell
docker run -it --rm eidoom/njet-test[:<tag>] bash
```

Alternative usage discussed on the [Rivet website](https://rivet.hepforge.org/trac/wiki/Docker) could be similarly used for NJet.

## Build

Build with
```shell
docker build -t eidoom/njet-test:<tag> [-t eidoom/njet-test:latest] .
```
then push to Docker Hub with
```shell
docker push eidoom/njet-test:<tag> 
[docker push eidoom/njet-test:latest]
```
To update the `latest` image separately,
```shell
docker tag eidoom/njet-test:<tag> eidoom/njet-test:latest
docker push eidoom/njet-test:latest
```

## See also

* [njet-container](https://gitlab.com/eidoom/njet-container)
* [njet-git-container](https://gitlab.com/eidoom/njet-git-container)
